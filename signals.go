package main

import (
	"os/exec"
	"syscall"
)

var sigstop syscall.Signal = syscall.SIGSTOP
var sigcont syscall.Signal = syscall.SIGCONT
var sigint syscall.Signal = syscall.SIGINT
var sigterm syscall.Signal = syscall.SIGTERM
var sigkill syscall.Signal = syscall.SIGKILL

// https://stackoverflow.com/a/33171307
func setupProcGroup(cmd *exec.Cmd) {
	cmd.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}
}
