package main

import (
	"bufio"
	// "encoding/hex"
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"os/exec"
	"os/signal"
	"runtime"
	"strings"
	"syscall"
	"time"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	runtime.GOMAXPROCS(runtime.NumCPU())

	var lp int           // listen port
	var dp int           // dest port
	var swait int        // wait time before sending SIGSTOP on last disconnect (seconds)
	var pausearg string  // string to send on last disconnect
	var sigintarg string // string to send on sigint
	var bindaddr string  // bindaddr
	var destaddr string  // dest addr
	var startuptime int  // wait time after starting before sending SIGSTOP (seconds)

	flag.IntVar(&lp, "lp", 7777, "port to listen on")
	flag.IntVar(&dp, "dp", 7778, "port to forward to")
	flag.IntVar(&swait, "wait", 30, "seconds before sending SIGSTOP on last disconnect")
	flag.StringVar(&pausearg, "pausearg", "save\n", "string to send on stdin on last disconnect")
	flag.StringVar(&sigintarg, "sigintarg", "exit\n", "string to send on stdin on sigint")
	flag.StringVar(&bindaddr, "bind", "0.0.0.0", "IPv4 address to bind")
	flag.StringVar(&destaddr, "dest", "127.0.0.1", "IPv4 address to forward to")
	flag.IntVar(&startuptime, "startuptime", 30, "seconds after starting before sending SIGSTOP")
	flag.Parse()

	program := flag.Args()
	log.Println(program)

	if len(program) < 1 {
		log.Fatal("no program")
	}

	signalCh := make(chan syscall.Signal)
	stdinCh := make(chan []byte)

	incomingSignalCh := make(chan os.Signal, 1)
	sigintCh := make(chan bool, 1)
	signal.Notify(incomingSignalCh, os.Interrupt)
	go func() {
		<-incomingSignalCh
		sigintCh <- true
		data := make([]byte, len(sigintarg))
		copy(data, sigintarg)
		stdinCh <- data
		<-incomingSignalCh
		log.Println("You are now 3 steps away from SIGINT-ing the subprocess")
		<-incomingSignalCh
		log.Println("You are now 2 steps away from SIGINT-ing the subprocess")
		<-incomingSignalCh
		log.Println("You are now 1 step away from SIGINT-ing the subprocess")
		<-incomingSignalCh
		signalCh <- sigcont
		signalCh <- sigint
		log.Println("You are now 1 step away from SIGTERM-ing the subprocess")
		<-incomingSignalCh
		signalCh <- sigterm
		log.Println("You are now 1 step away from SIGKILL-ing the subprocess")
		<-incomingSignalCh
		signalCh <- sigkill
		log.Println("You are now 1 step away from exit()-ing")
		<-incomingSignalCh
		os.Exit(1)
	}()

	cmd := exec.Command(program[0], program[1:]...)
	setupProcGroup(cmd)

	stdin, err := cmd.StdinPipe()
	if err != nil {
		log.Fatal("can't get stdin pipe,", err)
	}
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		log.Fatal("can't get stdout pipe,", err)
	}
	stderr, err := cmd.StderrPipe()
	if err != nil {
		log.Fatal("can't get stderr pipe,", err)
	}

	err = cmd.Start()
	if err != nil {
		log.Fatal("can't start program,", err)
	}

	cmdDoneCh := make(chan bool, 2)

	// stdout loop
	go func() {
		// <-time.After(time.Second)
		var buf [16384]byte
		for {
			n, err := stdout.Read(buf[:])
			if err != nil {
				if err != io.EOF {
					log.Println("read failed,", err)
				}
				break
			}

			wn := 0
			for wn < n {
				wndel, err := os.Stdout.Write(buf[wn:n])
				if err != nil {
					log.Println("write failed,", err)
					break
				}
				wn += wndel
			}
		}
		cmdDoneCh <- true
	}()

	// stderr loop
	go func() {
		// <-time.After(time.Second)
		var buf [16384]byte
		for {
			n, err := stderr.Read(buf[:])
			if err != nil {
				if err != io.EOF {
					log.Println("read failed,", err)
				}
				break
			}

			wn := 0
			for wn < n {
				wndel, err := os.Stderr.Write(buf[wn:n])
				if err != nil {
					log.Println("write failed,", err)
					break
				}
				wn += wndel
			}
		}
		cmdDoneCh <- true
	}()

	// go stdin loop
	go func() {
		rd := bufio.NewReaderSize(os.Stdin, 16384)
		for {
			buf, err := rd.ReadBytes('\n')
			if err != nil {
				log.Println("readline failed,", err)
				continue
			}
			n := len(buf)

			data := make([]byte, n)
			copy(data, buf[:n])
			stdinCh <- data
		}
	}()

	connectionCh := make(chan bool)
	disconnectCh := make(chan bool)

	// serialized subprocess stdin loop
	go func() {
		for {
			data, ok := <-stdinCh
			if !ok {
				continue
			}
			n := len(data)

			connectionCh <- true
			// fmt.Println("----" + hex.EncodeToString(data) + "----")

			wn := 0
			for wn < n {
				wndel, err := stdin.Write(data[wn:n])
				if err != nil {
					log.Println("write failed,", err)
					break
				}
				wn += wndel
			}

			disconnectCh <- false
		}
	}()

	// signal sender
	go func() {
		for {
			sig, ok := <-signalCh
			if !ok {
				continue
			}
			log.Println("sending sig:", sig)
			err = cmd.Process.Signal(sig)
			if err != nil {
				log.Println("couldn't send", sig, "to process")
			}
		}
	}()

	zeroConnCh := make(chan bool)
	newConnCh := make(chan bool)

	// connection counter
	go func() {
		connections := 0
		for {
			select {
			case <-connectionCh:
				connections++
				newConnCh <- true
			case s := <-disconnectCh:
				connections--
				if connections == 0 {
					zeroConnCh <- s // s is only false in the stdin processor
				}
			}
		}
	}()

	// timer stop loop
	go func() {
		for {
			select {
			case s := <-zeroConnCh:
				if s { // s is only false when we shouldn't auto-save, like in the stdin processor
					data := make([]byte, len(pausearg))
					copy(data, pausearg)
					stdinCh <- data
				}
				// go to the next select
			case <-newConnCh:
				signalCh <- sigcont
				continue
			}

			select {
			case <-time.After(time.Duration(swait) * time.Second):
				// send SIGSTOP
				signalCh <- sigstop
			case <-newConnCh:
				// do nothing
			}
		}
	}()

	log.Println("waiting startuptime (", startuptime, "sec )")
	select {
	case <-time.After(time.Duration(startuptime) * time.Second):
		signalCh <- sigstop
	case <-sigintCh:
		log.Println("interrutping wait")
	}

	// tcp listener
	go func() {
		lsn, err := net.Listen("tcp4", fmt.Sprintf("%s:%d", bindaddr, lp))
		if err != nil {
			log.Println("listener failed,", err)
			return
		}
		defer lsn.Close()

		for {
			gameclient, err := lsn.Accept()
			if err != nil {
				log.Println("accept failed,", err)
				continue
			}

			// signalCh <- sigcont

			// socat bit
			go func(gameclient net.Conn) {
				goclient, err := net.Dial("tcp4", fmt.Sprintf("%s:%d", destaddr, dp))
				if err != nil {
					log.Println("connect failed,", err)
					return
				}
				// Intentionally not defering close()
				// defer goclient.Close()

				// somehow magic happens and Dial works even when its destination has been SIGSTOP-ed
				connectionCh <- true

				rwchan := make(chan bool, 2)

				// reader for real server
				go func() {
					var buf [16384]byte
					for {
						n, err := goclient.Read(buf[:])
						if err != nil {
							// I close this connection when the other one dies, so I don't care if this one is closed
							if err != io.EOF && !strings.Contains(err.Error(), "use of closed network connection") {
								log.Println("read failed,", err)
							}
							break
						}

						wn := 0
						for wn < n {
							wndel, err := gameclient.Write(buf[:n])
							if err != nil {
								log.Println("write failed,", err)
								break
							}
							wn += wndel
						}
					}
					rwchan <- true
				}()

				// writer for real server
				go func() {
					var buf [16384]byte
					for {
						n, err := gameclient.Read(buf[:])
						if err != nil {
							// I want to see if this read fails for closed network connection
							if err != io.EOF {
								log.Println("read failed,", err)
							}
							break
						}

						wn := 0
						for wn < n {
							wndel, err := goclient.Write(buf[:n])
							if err != nil {
								log.Println("write failed,", err)
								break
							}
							wn += wndel
						}
					}
					rwchan <- true
				}()
				<-rwchan
				goclient.Close()
				<-rwchan
				close(rwchan)
				gameclient.Close()

				disconnectCh <- true

				// log.Println("disconnect")
			}(gameclient)
		}
	}()

	<-cmdDoneCh
	<-cmdDoneCh

	err = cmd.Wait()
	if err != nil {
		log.Fatal("err from wait, ", err)
	}
}
