sigsrv
======

## About
I run a terraria server on my AWS EC2 instance.  The tier of EC2 I have has a weird CPU time scheme where I make 12 credits / CPUcore / hour, and I spend 1 credit per minute of 100% usage / CPUcore.  These credits are not integers, I assume one credit at 50% CPU usage would last 2 minutes.

The terraria server runs at about 20% of one core when no one is logged in.  And about 50% of one core when one or more people are logged in.  The ~20% is just barely enough to keep making credits at a very slow rate.  At 50%, I lose credits pretty fast.  When we play on the server a lot, I can run out of credits and the CPU gets limited and the server starts lagging a lot.

I can stop and start the server everytime I want to use it, but it takes a bit of time to start up, plus I don't want to have to ssh in to the server every time.  Or I can be creative.

I wanted a thing I can connect to that forwards my network traffic and to the server, and when I disconnect, pauses(SIGSTOP) the server.  Then when I connect again, resumes(SIGCONT) the server.  Just killing and relaunching the process is annoying because of the launch delay.

## Usage
Build the code with go `go build -o sigsrv *.go` or with options to make it smaller `go build -o sigsrv -ldflags "-w -s" *.go`

`./sigsrv -h` will tell you:

```
Usage of ./sigsrv:
  -bind string
        IPv4 address to bind (default "0.0.0.0")
  -dest string
        IPv4 address to forward to (default "127.0.0.1")
  -dp int
        port to forward to (default 7778)
  -lp int
        port to listen on (default 7777)
  -pausearg string
        string to send on stdin on last disconnect (default "save\n")
  -sigintarg string
        string to send on stdin on sigint (default "exit\n")
  -startuptime int
        seconds after starting before sending SIGSTOP (default 30)
  -wait int
        seconds before sending SIGSTOP on last disconnect (default 30)
```

After any of those arguments, you can add `--` then a command to run.
Eg. `./sigsrv -startuptime 10 -wait 10 -- nc -l 7778 -k -vv`

## How it works
1. It runs the command specified
2. Waits `startuptime`
3. Connects commands's stdout & stderr to sigsrv's stdout & stderr
    1. stdin is special
    2. sigsrv's stdin is line buffered and sent to the command one line at a time
    3. sigsrv can automatically send strings to the command's stdin
        1. When the last person disconnects
        2. When you ctrl-C sigsrv
4. Launches a TCP server
    1. listen on `bind`:`lp`
5. On connect
    1. Send SIGCONT to command
    2. Connect to command's server
    3. Launch goroutine to handle forwarding data client->command `dest`:`dp`
    4. Launch goroutine to handle forwarding data command->client
6. On last disconnect
    1. Send `pausarg` to stdin
    2. Wait `wait`
    3. Send SIGSTOP to command
7. When sigsrv recieves SIGINT, it sends `sigintarg` to command's stdin
    1. If this doesn't stop the command, you can keep sending SIGINT to sigsrv to make it send SIGINT, then SIGTERM, then SIGKILL to command.
8. Once command exits, sigsrv stops
